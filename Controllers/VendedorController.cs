using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class VendedorController : Controller
    {
        private readonly VendasContext _context;

        public VendedorController(VendasContext context)
        {
            _context = context;
        }

        //Método Create
        [HttpPost]
        public IActionResult Create(Vendedor vendedor)
        {
            if (vendedor.Id == 0)
            {
                _context.Vendedores.Add(vendedor);
            }
            else
            {
                var vendedorInDb = _context.Vendedores.Find(vendedor.Id);

                if (vendedorInDb == null)
                    return NotFound();

                vendedorInDb = vendedor;
            }

            _context.SaveChanges();

            return Ok(vendedor);
        }

        //Método Get
        [HttpGet]
        public IActionResult Get(int id)
        {
            var result = _context.Vendedores.Find(id);

            if (result == null)
                return NotFound();

            return (Ok(result));
        }

        //Método Delete
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var result = _context.Vendedores.Find(id);

            if (result == null)
                return NotFound();

            _context.Vendedores.Remove(result);
            _context.SaveChanges();

            return NoContent();
        }

        //Método Put
        [HttpPut]
        public IActionResult Edit(Vendedor vendedor)
        {
            var vendedorInDb = _context.Vendedores.Find(vendedor.Id);
            if (vendedorInDb == null)
                return NotFound();

            _context.Entry(vendedorInDb).State = Microsoft.EntityFrameworkCore.EntityState.Detached;

            //Condição que impede o valor de ser alterado caso seja 0(no caso do Telefone), ou caso seja nulo(Cpf, Nome, Email)
            //Condição Cpf
            if (vendedor.Cpf != null)
            {
                vendedorInDb.Cpf = vendedor.Cpf;
            }
            else
            {
                vendedor.Cpf = vendedorInDb.Cpf;
            }

            //Condição Nome
            if (vendedor.Nome != null)
            {
                vendedorInDb.Nome = vendedor.Nome;
            }
            else
            {
                vendedor.Nome = vendedorInDb.Nome;
            }

            //Condição Email
            if (vendedor.Email != null)
            {
                vendedorInDb.Email = vendedor.Email;
            }
            else
            {
                vendedor.Email = vendedorInDb.Email;
            }

            //Condição Telefone
            if (vendedor.Telefone != 0)
            {
                vendedorInDb.Telefone = vendedor.Telefone;
            }
            else
            {
                vendedor.Telefone = vendedorInDb.Telefone;
            }

            _context.Update(vendedorInDb);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        //Método Getall
        [HttpGet()]
        public IActionResult GetAll()
        {
            var result = _context.Vendedores.ToList();

            return (Ok(result));
        }
    }
}