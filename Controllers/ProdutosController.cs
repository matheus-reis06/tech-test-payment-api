using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ProdutosController : Controller
    {
        private readonly VendasContext _context;

        public ProdutosController(VendasContext context)
        {
            _context = context;
        }

        //Método Create
        [HttpPost]
        public IActionResult Create(Produto produto)
        {
            if (produto.Id == 0)
            {
                _context.Produtos.Add(produto);
            }
            else
            {
                var produtoInDb = _context.Produtos.Find(produto.Id);

                if (produtoInDb == null)
                    return NotFound();

                produtoInDb = produto;
            }

            _context.SaveChanges();

            return Ok(produto);
        }

        //Método Get
        [HttpGet]
        public IActionResult Get(int id)
        {
            var result = _context.Produtos.Find(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        //Método Delete
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var result = _context.Produtos.Find(id);

            if (result == null)
                return NotFound();

            _context.Produtos.Remove(result);
            _context.SaveChanges();

            return NoContent();
        }

        //Método Put
        [HttpPut]
        public IActionResult Edit(Produto produto)
        {
            var produtoInDb = _context.Produtos.Find(produto.Id);
            if (produtoInDb == null)
                return NotFound();

            _context.Entry(produtoInDb).State = Microsoft.EntityFrameworkCore.EntityState.Detached;

            //Condição que impede o valor de ser alterado caso seje 0(no caso dos Id's), ou caso seje nulo(Nome do produto)
            //Condião IdVendedor
            if (produto.IdVendedor != 0)
            {
                produtoInDb.IdVendedor = produto.IdVendedor;
            }
            else
            {
                produto.IdVendedor = produtoInDb.IdVendedor;
            }

            //Condição NomeProduto
            if (produto.NomeProduto != null)
            {
                produtoInDb.NomeProduto = produto.NomeProduto;
            }
            else
            {
                produto.NomeProduto = produtoInDb.NomeProduto;
            }

            _context.Update(produtoInDb);
            _context.SaveChanges();
            return Ok(produto);
        }

        //Método GetAll
        [HttpGet()]
        public IActionResult GetAll()
        {
            var result = _context.Produtos.ToList();

            return Ok(result);
        }
    }
}