using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class VendasController : Controller
    {
        private readonly VendasContext _context;
        Random random = new Random(); //Utilizado no identificador

        public VendasController(VendasContext context)
        {
            _context = context;
        }

        //Gera Caracteres aleatórios para o Identificador     
        public static string GenerateRandomString(Random random, int stringLength)
        {
            const string allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            var randomString = new string(Enumerable.Repeat(allowedChars, stringLength)
            .Select(a => a[random.Next(a.Length)]).ToArray());
            return randomString;
        }

        //Método Create 
        [HttpPost]
        public IActionResult Create(Venda venda)
        {

            if (venda.Id == 0)
            {
                if (venda.Quantidade == 0)
                    return NoContent();

                venda.Data = DateTime.Now;
                venda.Status = EnumStatus.AguardandoPagamento;
                venda.Identificador = GenerateRandomString(random, 20);
                _context.Vendas.Add(venda);
            }
            else
            {
                var vendaInDb = _context.Vendas.Find(venda.Id);

                if (vendaInDb == null)
                    return NotFound();

                vendaInDb = venda;
            }
            _context.SaveChanges();
            return Ok(venda);
        }

        //Método Get 
        [HttpGet]
        public IActionResult Get(int id)
        {
            var result = _context.Vendas.Find(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        //Método Delete 
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var result = _context.Vendas.Find(id);

            if (result == null)
                return NotFound();

            _context.Vendas.Remove(result);
            _context.SaveChanges();

            return NoContent();
        }

        //Método Put
        [HttpPut]
        public IActionResult Edit(Venda venda)
        {
            var vendaInDb = _context.Vendas.Find(venda.Id);
            if (vendaInDb == null)
                return NotFound();

            _context.Entry(vendaInDb).State = Microsoft.EntityFrameworkCore.EntityState.Detached;

            //Condição que impede o valor de ser alterado caso seja 0(no caso dos Id's e do Status)
            //Condição Status
            if (venda.Status != 0)
            {
                vendaInDb.Status = venda.Status;
            }
            else
            {
                venda.Status = vendaInDb.Status;
            }

            //Condição IdVendedor
            if (venda.IdVendedor != 0)
            {
                vendaInDb.IdVendedor = venda.IdVendedor;
            }
            else
            {
                venda.IdVendedor = vendaInDb.IdVendedor;
            }

            //Condição IdProduto
            if (venda.IdProduto != 0)
            {
                vendaInDb.IdProduto = venda.IdProduto;
            }
            else
            {
                venda.IdProduto = vendaInDb.IdProduto;
            }

            _context.Update(vendaInDb);
            _context.SaveChanges();
            return Ok(venda);
        }

        //Método GetAll
        [HttpGet()]
        public IActionResult GetAll()
        {
            var result = _context.Vendas.ToList();

            return Ok(result);
        }


    }
}