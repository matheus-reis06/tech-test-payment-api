using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    //Enum que tem como papel indicar o status da venda
    public enum EnumStatus
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento = 1,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado = 2,
        [Description("Enviado para Transportadora")]
        EnviadoParaTransportadora = 3,
        [Description("Entregue")]
        Entregue = 4,
        [Description("Compra Cancelada")]
        Cancelada = 5

    }

}
