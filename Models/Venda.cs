using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Identificador { get; set; }
        public EnumStatus Status { get; set; }
        public int Quantidade { get; set; }
        public int IdVendedor { get; set; }
        public int IdProduto { get; set; }
    }
}